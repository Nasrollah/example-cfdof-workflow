
These tools can be helpfull under Linux and Windows.  
When you use the WSL (Windows Subsystem for Linux) it might be good to install them in both enviroments

# git 
For Windows you have to download [git](https://git-scm.com/downloads) from their website and install it.  
Under Linux or within the WSL you can execute following command to install git: 

    sudo apt-get install git 
    sudo apt-get install gitk
    sudo apt-get install git-gui

# VS Code
[Microsoft VS Code for Linux](https://code.visualstudio.com/docs/setup/linux) can be installed in the command line: 

    sudo snap install --classic code

[Microsoft VS Code for Windows](https://code.visualstudio.com/docs/setup/windows) is described here. 


# Small Linux console tools 
These tools are not necessary but helpfull in the Linux Terminal. 

    sudo apt-get install tree

